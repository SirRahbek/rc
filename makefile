SRC  = src
BIN  = bin
OUT  = rc
CXX  = c++
TEST = test

all:
	$(CXX) $(SRC)/rc.cpp -std=c++11 -s -g -o $(BIN)/$(OUT)

test:
	$(BIN)/$(OUT) $(TEST)/basic.rc


.PHONY: test