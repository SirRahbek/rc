# rc
A rudimentary compiler

##A small code snippet.
```go
func main (v64 args, v64 argv)
    v64 str = "Hello, world!\n";
    v64 pointer = func some_function (v64 string)
        printf("some string to print: %s\n", string);
        ret;;;
    pointer(str);
    ret 0;;
```
    
The compiler is invoked with the command rc.
Build process is the standard `make` and `make install`.

##A purposely stupid compiler.
This compiler has been made to show how compilers work in the most simple manner possible. Therefore the functionality of the compiler has been kept to a bare minimum. It does not link programs, it does not have any built in libraries, it does nothing magic you cannot easily deduce how should work on the assembler level (given you know assembler, that is).
Calling it a compiler might be a little overstated, as the program itself generates Intel style assembly code, which should then be compiled and linked.
Note that the compilers output can use the c library, however you will first have to link to libc, and your c compilers start and end functions to use these functions. An example makefile for a basic project that uses libc, can be found in the examples folder.

##A rudimentary syntax.
As a side idea, somewhat inspired by Python, the syntax has been kept as simple as i could. The language is not based arould indentation, though at first glance it may look like so. Every statement is ended with a semicolon, be it a simple assignment, a control structure, or an entire function. Note the function 'lambda_function' in the example above ends with three semicolons, the first colon ends the return statement, the next one ends the function, and the last one ends the assignment to the variable 'pointer'.

##Data the PC understands.
To make it easier to understand how the code executes on the processor, every datatype in rc is a datatype also present on the processor, therefore there is no such thing as a pointer, a string, or a char. The only type are 8, 16, 32 and 64bit variables. Because of the early stage of development, the compiler is locked to the intel x86 and 64 architectures for now. Switching between the two archicetures can be done by changing the string 'arch' in the code, between 86 and 64. This also means that there is no type safety whatsoever. If you assign a function pointer to a variable, and after a while increment that variable with some value, you will wind up someplace in the function if you call it. 
