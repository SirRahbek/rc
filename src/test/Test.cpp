#include "../Error.hpp"
#include "../Options.hpp"
#include "../Compiler.hpp"
#include <string>

void testError (string msg, int code) {
   std::cout << GREEN("[[") << RED(msg) << GREEN("]]") << std::endl;
	exit(code);
}

Compiler* rc;


bool test1 () {
   std::cout << rc->compile( SyntaxTree("func main () ret;;") );
	return false;
}



int main () {
	rc = new Compiler();

	std::cout << "[[Testing example code]]" << std::endl;
	if (test1())
		testError("Test 1 failed", 1);

	getchar();
	return 0;
}




