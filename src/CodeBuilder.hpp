#pragma once

#include <string>
#include <map>
#include "Options.hpp"
using namespace std;

string tab(int depth) {
	string output;
	for(int i=0 ; i<depth ; ++i)
		output += "\t";
	return output;
}

string str(string input, int depth)
	{ return tab(depth) + input + "\n"; }

string comment(string input, int depth)
	{ return tab(depth) + string("#") + input + "\n"; }

string opCode(string op, int depth){
	return tab(depth) + op + "\n"; }



string opReg(string op, string r, int depth)
	{ return tab(depth) + op + " " + reg[r] + "\n"; }

string opRegStr(string op, string r, string str, int depth)
	{ return tab(depth) + op + " " + reg[r] + ", " + str + "\n"; }

string opRegReg(string op, string r1, string r2, int depth)
	{ return tab(depth) + op + " " + reg[r1] + ", " + reg[r2] + "\n"; }

string opRefReg(string op, string r1, string r2, int depth)
	{ return tab(depth) + op + " [" + reg[r1] + "], " + reg[r2] + "\n"; }



string pushReg(string r, int depth)
	{ return tab(depth) + "push " + reg[r] + "\n"; }

string popReg(string r, int depth)
	{ return tab(depth) + "pop " + reg[r] + "\n"; }

string pushStr(string str, int depth)
	{ return tab(depth) + "push " + str + "\n"; }

string popStr(string str, int depth)
	{ return tab(depth) + "pop " + str + "\n"; }



string movRegReg(string r1, string r2, int depth)
	{ return tab(depth) + "mov " + reg[r1] + ", " + reg[r2] + "\n"; }

string movRegStr(string r, string str, int depth)
	{ return tab(depth) + "mov " + reg[r] + ", " + str + "\n"; }

string movStrReg(string str, string r, int depth)
	{ return tab(depth) + "mov " + str + ", " + reg[r] + "\n"; }

string movRefReg(string r1, string r2, int depth)
	{ return tab(depth) + "mov [" + reg[r1] + "], " + reg[r2] + "\n"; }

string movRegRef(string r, string str, int depth)
	{ return tab(depth) + "mov " + reg[r] + ", [" + str + "]\n"; }

string movRefRef(string r1, string r2, int depth)
	{ return tab(depth) + "mov [" + reg[r1] + "], [" + reg[r2] + "]\n"; }
