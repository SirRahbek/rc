#pragma once

#include <string>
#include "Options.hpp"

class Var {
public:
	int 		stackOffset;
	int 		size;
	string 	name;
	string	type;
	string	reference;

	Var () {
		return;
	}

	Var (string type, string name, int offset=0) {
		this->type 		= type;
		this->name 		= name;
		//TODO: this needs to be fixed for the 'pt' datatype.
		this->size		= atoi(&type[1])/8;
		stackOffset 	= offset;
		reference		= reg["bp"] + "-" + to_string(stackOffset);
		return;
	}

	Var (string type, string name, int size, int offset) { //for structs.
		this->name 		= name;
		this->type 		= type;
		this->size 		= size;
		stackOffset 	= offset;
		reference		= reg["bp"] + "-" + to_string(stackOffset);
		return;
	}

	Var (string name) { //only for use with string identified pointers (functions, externs, ...)
		this->name 	= name;
		this->type 	= "function";
		this->size 	= (options.arch=="86") ? 4 : 8;
		this->stackOffset = 0;
		reference	= name;
		return;
	}
};
