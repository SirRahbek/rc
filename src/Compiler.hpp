#pragma once

#include <string>
#include <vector>
#include <algorithm>
#include <map>
#include "SyntaxTree.hpp"
#include "Var.hpp"
#include "Struct.hpp"
#include "CodeBuilder.hpp"
#include "Options.hpp"

using namespace std;

class Compiler
{
	int ifN, whileN, forN, strN, depth;
	SyntaxTree 				code;
	map<string,string>	functions;
	map<string,string>	strings;
	map<string,Struct*>	structs;
	vector<string>			externs;

public:
	enum STATES {
		ONCE				= true,
		NOT_ONCE			= false,
		NOT_EVEN_ONCE	= 2	//Meth
	};

	Compiler ()
	{
		ifN = whileN = forN = strN = 0;
		return;
	}


	/**	the compile function, takes a syntaxTree as input, which it stores
	 *		in the class, that the other functions then use to generate code with.
	 *		It does not itself make any code, but rather calls makeCode, which
	 *		then generates some code, which is stored in some functions.
	 */
	string compile (SyntaxTree code)
	{
		this->code = code;
		string 				output;
		map<string,Var>	vars;
		int stackSize = 0;

		makeCode(vars, &stackSize, 0, NOT_ONCE);


		output += "section .text\n\tglobal main\n";
		for (string ext : externs)
			output += "\textern " + ext + ";\n";
		output += "\n";

		for (auto& f : functions)
			output += f.second + "\n";

		if (strings.size() > 0)
			output += "section .data\n";
		for (auto& s : strings)
			output += "\t"+s.first+" db `"+s.second+"`, 0\n";
		return "\n"+output;
	};


	/**	makeCode is the primary function for generating the assembly code.
	 *		it outputs the generated code as a c++ string.
	 */
	string makeCode (map<string,Var> context, int* stackSize, int depth, bool once=false)
	{
		string output;
		map<string,Var> vars = context;
		Token * initToken = code.peek(0);
		Token * t = NULL;

		//should be range based.
		for (t=code.getNext() ; t != NULL ; t=code.getNext()) {
			if (once)
				prelog("makecode-once", initToken, depth);
			else
				prelog("makecode", initToken, depth);

			switch (t->type) {
				case SyntaxTree::KEYWORD:	/*Keyword*/
					if (t->data.compare("ret")==0) { //TODO: maybe move this block to makeFunctionTail?
						if (!code.peek()->match('+', ";"))
							output += makeCode(vars, stackSize, depth, ONCE);
						else
							code.getNext();
						output += makeFunctionTail(depth);
					}
					else if (t->data == "func") {
						string funcName = code.peek()->data;
						makeFunction(vars);
						if (once)
							output += movRegRef("ax", funcName, depth);
					}
					else if (t->data == "if") {
						output += buildIfStatement(vars,stackSize,depth);
					}
					else if (t->data == "while") {
						output += buildWhileStatement(vars,stackSize,depth);
					}
					else if (t->data == "struct") {
						output += defineStruct();
					}
					else {
						error("no operation for keyword", t);
					}
					break;

				case SyntaxTree::TYPE:	/*Variable*/
					output += createVariables(vars, stackSize, depth);
					break;

				case SyntaxTree::OPERATOR:	/*Operator*/
					if (t->data.compare(";")==0 || t->data.compare(")")==0) {
						log("! makecode", initToken, depth);
						return output;
						break;
					}
					//If this operator is encountered, result is actually a value, treat as such.
					if (t->data.compare("(") == 0) {
						output += pushReg("ax", depth);
						output += makeCode(vars, stackSize, depth, NOT_ONCE);
						output += popReg("bx", depth);
						output += treatOperator(vars, stackSize, depth);
						break;
					}
					/* Search for ++ or -- */
					/*
					if (unaryOp.count(t->data) > 0) {
						log("unary operator", t, depth);
						string assignment = unaryOp[t->data];
						t = code.getNext();
						output += str(assignment + " " + processDataType(vars), depth);
						t = code.getNext();
						if (t->match(SyntaxTree::OPERATOR,";")) //what?
							break;
					}
					*/
					else {
						error("No action for operator", t);
					}

				case SyntaxTree::VARIABLE:		// As for now,
				case SyntaxTree::STRING:		// all types of data act the same.
				case SyntaxTree::NUMBER:		//		#AllDataAreEqual
					//See if keyword is a defined struct.
					if (structs.count(t->data) >= 1) {
                  output += createStructs(vars, stackSize, depth);
						break;
					}
					if (varIsResult() && !once)
						output += fetchAddress("ax",vars,depth);
					else
						output += fetchValue("ax",vars,depth);
					output += treatOperator(vars, stackSize, depth);
					break;
			}
			if(once) break;
		}
		log("! makecode", initToken, depth);
		return output;
	}


	/**	the makeFunction, creates a function, by generating a string,
	 *		which is then saved with its name in a map.
	 */
	void makeFunction (map<string,Var> context)
	{
		Token* initToken = code.peek(0);
		log("makefunction", initToken, 0);

		map<string,Var> vars = context;
		int stackSize = 8;
		int argN = 0;
		int argumentFrame = 0;
		string content = "";
		string alignArguments = "";
		string name = code.getNext()->data;
		functions[name] = code.peek(0)->data; //make visible to compiler.

		Token* t = code.getNext();
		if ( !t->match('+',"(") )
			error("Expected (", t);

		while((t=code.getNext()) != NULL) {
			string varName;
			Token* varType = t;
			Var* arg;

			if (t->match('+',")"))
				break;
			if (t->match('+',","))
				continue;
			if ( !(t->type == 't'))
				error("expected type or struct", t);

			//For all arguments that are passed in registers.
			if (argN < intArgumentOrder.size()) {
				log("making function argument", code.peek());

				if (code.peek()->match('+',"&")) { //its a reference to a struct.
					log("its a pointer", code.peek());
					varName = code.getNext()->data;
					int varSize = (options.arch=="86") ? 4:8;
					arg = new Var(varType->data, varName, varSize, stackSize);
				}/* TODO: add struct support.
				else if(varType->data in structs) {
					varName = code.getNext()->data;
					Struct st = structs[varType.data];
					arg = new Var(varType.data, varName, st.size, stackSize);
					stackSize += st.size;
				}*/
				else if (varType->type == 't') {
					log("making ordinary argument", code.peek());
					arg = new Var(varType->data, code.getNext()->data, stackSize);
				}
				else
					error("not a struct or type", t);

				vars[arg->name] = *arg;
				stackSize += arg->size;
				alignArguments += str("mov "+arg->reference+", "+intArgumentOrder[argN], 1);
				argN++;	//the argN stone... ha ha ha.. ha.
			}
			//For all arguments that are passed on the stack.
			else { //hope this wont be called anytime soon.
				arg = new Var(t->data, varName, stackSize);
				argumentFrame -= arg->size;
				arg->stackOffset = argumentFrame;
				arg->reference = "["+reg["bp"]+"+"+to_string(argumentFrame)+"]";
				vars[arg->name] = *arg;
			}
		}

		content += str(name+":", 0);
		content += makeFunctionHead(stackSize, 1);
		content += alignArguments;
		content += makeCode(vars, &stackSize, 1, NOT_ONCE);

		//t = code.getNext();
		//if (!t->match('+', ";"))
		//	error("Expected semicolon after function end", t);

		functions[name] = content;
		log("! makefunction", initToken);
		return;
	}


	string makeFunctionHead (int stack, int depth)
	{
		string output;
		output += pushReg("bp", depth);
		output += movRegReg("bp", "sp", depth);
		output += opRegStr("sub", "sp", to_string(stack), depth);
		return output;
	}


	string makeFunctionTail (int depth)
	{
		string output;
		output += movRegReg("sp", "bp", depth);
		output += popReg("bp", depth);
		output += opCode("ret", depth);
		return output;
	}


	/**	The buildIfStatement function, takes the next tokens in the syntax tree,
	 *		and creates a block of code, that will only be executed on some condition
	 */
	string buildIfStatement (map<string,Var> vars, int* stackSize, int depth)
	{
		string output;
		int thisIf = ifN++;

		output += str(".if" + to_string(thisIf) + ":", depth);
		code.getNext();
		log("buildif-condition", code.peek(0), depth);
		output += makeCode(vars,stackSize,depth+1, ONCE);
		output += str("cmp " + reg["ax"] + ", 0", depth+1);
		output += str("je .no_if"+to_string(thisIf), depth+1);

		output += str(".yes_if" + to_string(thisIf) + ":", depth);
		log("buildif-expression", code.peek(0), depth);
		output += makeCode(vars,stackSize,depth+1, NOT_ONCE);

		Token* x = code.peek();
		output += str("jmp .end_if" + to_string(thisIf), depth+1);
		output += str(".no_if" + to_string(thisIf) + ":", depth);
		if (x != NULL && x->match('k', "else")) {
			code.getNext();
			output += makeCode(vars,stackSize,depth+1);
		}
		output += str(".end_if"+to_string(thisIf)+":", depth);
		return output;
	}


	/**	The buildWhileStatement takes a single expression of code, and builds
	 *		a condition on which it will execute the code until the next double
	 *		statement closer.
	 */
	string buildWhileStatement (map<string,Var> vars, int* stackSize, int depth)
	{
		string output = "";
		int thisWhile = whileN++;

		code.getNext();
		output += str(".while" + to_string(thisWhile) + ":", depth);
		log("buildwhile-condition", code.peek(), depth);
		output += makeCode(vars,stackSize,depth+1, true);
		code.revert();
		output += str("cmp " + reg["ax"] + ", 0", depth+1);
		output += str("je .while_end" + to_string(thisWhile), depth+1);

		output += str(".while_loop" + to_string(thisWhile) + ":", depth);
		code.getNext(); //Go past the ')'
		log("buildwhile-expression", code.peek(), depth);
		output += makeCode(vars,stackSize,depth+1);
		output += str("jmp .while" + to_string(thisWhile), depth+1);
		output += str(".while_end" + to_string(thisWhile) + ":", depth);
		return output;
	}


	/**	buildFunctionCall, makes a call to a function, by taking a list
	 *		of arguments, and pushing them to the stack, or in appropriate registers
	 *		and then calls the function.
	 */
	string buildFunctionCall (map<string,Var> context, int stackSize, int depth) //TODO: Add x86 compability.
	{
		prelog("function call", code.peek(-1), depth);
		string output;
		Token* t;
		vector<string> argumentList;
		while ((t=code.getNext()) != NULL && !t->match('+', ")")) {
			prelog("argument", t, depth);
			if (t->match('+',","))
				continue;
			code.revert();
			string temp = makeCode(context, &stackSize, depth, ONCE);
			code.revert();
			argumentList.push_back(temp);
			printf("%s", temp.c_str());
		}

		output += pushReg("ax", depth);

		int regIndex = 0;
		reverse(argumentList.begin(), argumentList.end());
		for (string s : argumentList) {
			output += s;
			if (regIndex < intArgumentOrder.size())
				output += movStrReg(intArgumentOrder.at(regIndex++).c_str(), "ax", depth);
			else
				output += pushReg("ax", depth);
		}

		output += movRegStr("ax", "0", depth);
		output += popReg("bx", depth);
		output += opReg("call", "bx", depth);
		log("! function call", code.peek(), depth);
		return output;
	}


	/**	createVariables takes a pointer to a map of variables, and assigns
	 *		new variables to it, by iterating throgh a list of names, prepended
	 *		by some type specifier.
	 */
	string createVariables (map<string,Var> vars, int* stackSize, int depth)
	{
		Token* initToken = code.peek(-1);
		log("createvariables", initToken, depth);
		string	output;
		int 		varSize;
		Var 		var;
		Token*	t;
		string 	typeName = code.peek(-1)->data;

		//First, check if we should make pointers.
		if (code.peek()->match('+', "&") || code.peek(-1)->match('t', "pt")) {
			code.getNext();
			varSize = options.ptSize;
		}
		else
			varSize = stoi(&(typeName[1])) / 8;

		while ((t=code.getNext()) != NULL) {
			if (t->type != 'v') {
				if(t->match('+',";"))
					break;
				else if (t->match('+',","))
					continue;
				else
					error("No operation for token", t);
			}

			Var v(typeName, t->data, varSize, *stackSize);
			*stackSize += varSize;
			vars[v.name] = v;
			/* TODO: support for value init */
			if (code.peek()->match('+', "=")) {
				code.revert();
				output += makeCode(vars, stackSize, depth, ONCE);
				code.revert();
			}
		}
		log("! createvariables", initToken, depth);
		return output;
	}

	/**	createStructs creates an instance of a struct.
	 */
	string createStructs (map<string,Var> vars, int* stackSize, int depth) {
		code.revert();
		Token* tok = code.getNext();
		log("creating structs", tok, depth);
		Token* t;
		int varSize;
		string output;
		string structName = tok->data;
		Struct* st = structs[structName];

		//make pointers.
		if(code.peek()->match('+',"&")) {
			code.getNext();
			varSize = options.ptSize;
		}
		else
			varSize = st->size;

		while( (t = code.getNext()) != NULL) {
			if(t->type == 'v') {
				Var v(tok->data, t->data, varSize, *stackSize);
				(*stackSize) += varSize;
				vars[v.name] = v;
				Token* op = code.getNext();
				if(op->match('+',"=") ) {
					output += makeCode(vars, stackSize, depth, ONCE);
					code.revert();
					output += movRefReg(v.reference, "ax", depth);
					continue;
				}
				else if(op->match('+',";")) //TODO: does this comply to the spec?
					break;
				else
					error("expected comma or assignment", op);
			}
			else if( t->match('+',";") )
				break;
			else if( t->match('+',",") )
				continue;
			else
				error("unexpected token", t);
		}
		return output;
	}

	/**	defineStruct is the function called whenever the compiler sees the keyword 'struct'
	 *		It starts an expression (a block ended with a ;), and defines the struct as all the elements
	 *		that are defined inside that block. (at least the variables).
	 */
	string defineStruct () {
		string output;
		Token* t = code.getNext();

		if (t == NULL) {
			error("unexpected end of input", t);
			return output;
		}
		if (t->type != SyntaxTree::VARIABLE) {
			error("Expected a name for the struct", t);
			return output;
		}
		string 				name = t->data;
		int					size = 0;
		map<string,Var> 	vars;

		//here i get the type.
		while ((t=code.getNext()) != NULL) {
			if (t->match('+', ";"))
				break;
			if (t->type != SyntaxTree::TYPE) {
				error("expected identifier", t);
				return output;
			}
			//Here we get the actual variables.
			Token* t2;
			while ((t2=code.getNext()) != NULL) {
            if (t2->match('+', ";"))
					break;
				else if (t2->match('+', ","))
					continue;
				else if (t2->type != SyntaxTree::VARIABLE) {
					error("expected identifier or expression end", t2);
					break;
				}
				Var v(t->data, t2->data, size);
				size += v.size;
				vars[t2->data] = v;
			}
			structs[name] = new Struct(vars);
		}

		printf("[struct %s:\n", name.c_str());
		for (auto entry : structs[name]->vars) {
			printf("\t%s %s;\n", entry.second.type.c_str(), entry.first.c_str());
		}
		printf("]\n");

		return output;
	}


	/**	fetchData evaluates the next token in the syntaxTree, and tries to infer
	 *		the address of the variables, or returns its name if it fails to do so.
	 */
	string fetchAddress (string r, map<string,Var> vars, int depth)
	{
		Token* t = code.peek(-1);
		log("fetchaddr", t, depth);

		if (t->type == '0') {
			return movRegStr(r, t->data, depth);
		}
		else if (t->type == 'v') {
			if (vars.count(t->data) > 0)
				return movRegStr(r, vars[t->data].reference, depth);
			else
				return movRegStr(r, t->data, depth);
		}
		else
			error("Cannot extract data from this type", t);

		return std::string();
	}


	/**	fetchData evaluates the next token in the syntaxTree, and tries to infer
	 *		the value of the variables, or returns its name if it fails to do so.
	 */
	string fetchValue (string r, map<string,Var> vars, int depth)
	{
		Token* t = code.peek(-1);
		log("fetchval", t, depth);

		if (t->type == '0') {
			return movRegStr(r, t->data, depth);
		}
		else if (t->type == 'v') {
			if (vars.count(t->data) > 0)
				return movRegRef(r, vars[t->data].reference, depth);
			else
				return movRegRef(r, t->data, depth);
		}
		else
			error("Cannot extract data from this type", t);

		return std::string();
	}


	string processDataType (map<string,Var> vars)
	{
		Token* t = code.peek(-1);
		if (t->type == 'v' && vars.count(t->data) > 0)
			return "[" + vars[t->data].reference + "]";
		else
			return t->data;
	}


	string treatOperator (map<string,Var> vars, int* stackSize, int depth)
	{
		Token* t = code.getNext();
		string output = "";
		if (t == NULL) {
			//preeety sure i should make an error here.
			error("Unexpected end of input", new Token());
			return NULL;
		}

		//check if data is a function call. Repeat as long as valid.
		while (t->match('+', "(")) {
			output += buildFunctionCall(vars, *stackSize, depth);
			t = code.getNext();
		}

		if (t->match('+', ";")) {
			return output;
		}
		else if (opCompare.count(t->data) > 0) { //check whether token is an comparator ie: == != >= ...
			string comparison = opCompare[t->data];
			output += pushReg("ax", depth);
			output += makeCode(vars, stackSize, depth, ONCE);
			output += popReg("bx", depth);
			output += opRegReg("xchg", "ax", "bx", depth);
			output += opRegReg("sub", "ax", "bx", depth);
			output += movRegStr("bx", "1", depth);
			output += opReg(comparison, "bx", depth);		//TODO: this should be optimizable...
		}
		else if (assign.count(t->data) > 0) { //check whether token is an assigner ie: += *= = ...
			string assignment = assign[t->data];
			output += pushReg("ax", depth);
			output += makeCode(vars, stackSize, depth, ONCE);
			output += popReg("bx", depth);
			output += opRefReg(assignment, "bx", "ax", depth);
		}
		else if (op.count(t->data) > 0) { //check whether token is an infix operator: + - * / ...
			string operation = op[t->data];
			output += pushReg("ax", depth);
			output += makeCode(vars, stackSize, depth, ONCE);
			output += popReg("bx", depth);
			output += opReg(operation, "bx", depth);
		}
		return output;
	}


	bool isData (char data)
	{
		return (data=='0' || data=='v' || data=='s');
	}

	bool varIsResult ()
	{
		for (int n=0 ; code.peek(n) != NULL ; ++n) {
			Token* t = code.peek(n);
			if (t->match('+',";"))
				return false;
			if (assign.count(t->data) > 0)
				return true;
		}
		return false;
	}

};

