#ifndef OPTIONS_HEADER
#define OPTIONS_HEADER

#include <string>
#include <vector>
#include <map>

using namespace std;


map<string, string> reg;
map<string, string> op;
map<string, string> opCompare;

vector<string> intArgumentOrder = {
	"rdi" , "rsi" , "rdx" , "rcx" , "r8" , "r9"
};

map<string, string> assign = {
		{"="  , "mov"} , {"+=" , "add"} ,
		{"-=" , "sub"} , {"&=" , "and"} ,
		{"|=" , "or "}
};

map<string, string> unaryOp = {
		{"++" , "inc"} , {"--" , "dec"}
};

//Here is a table over the operator precedence for rc.
// higher numbers are evaluated first.
vector<string> opPred = {"|", "&", "+", "-", "*", "/", "%%"};

class Options
{
public:
	string 	arch, output, input;
	bool 		verbose, link;
	int 		ptSize;

	Options (string arch, string output)
	{
		this->arch 	= arch;
		this->output = output;
		verbose = false;
		//I should be punched for not doing this with polymorphism or composites.. oh well, it works.
		if (arch=="86") {
			ptSize = 4;
			reg = {
				{"ax" , "eax"} , {"bx" , "ebx"} , {"cx" , "ecx"} , {"dx" , "edx"} ,
				{"sp" , "esp"} , {"bp" , "ebp"} , {"si" , "esi"} , {"di" , "edi"}
			};

			op = {
				{"+" , "add eax,"} , {"-" , "sub eax,"},
				{"&" , "and eax,"} , {"|" , "or eax," },
				{"*" , "mul"}      , {"/" , "div"}
			};

		} else {
			ptSize = 8;
			reg = {
				{"ax" , "rax"} , {"bx" , "rbx"} , {"cx" , "rcx"} , {"dx" , "rdx"} ,
				{"sp" , "rsp"} , {"bp" , "rbp"} , {"si" , "rsi"} , {"di" , "rdi"}
			};

			op = {
				{"+"  , "add rax,"} , 		{"-"  , "sub rax,"},
				{"&"  , "and rax,"} , 		{"|"  , "or rax," },
				{"*"  , "mul"} ,      		{"/"  , "div"}
			};

			opCompare = {
				{"==" , "cmovz rax, "} , 	{"!=" , "cmovnz rax,"},
				{">"  , "cmova rax, "} , 	{"<"  , "cmovb rax,"},
				{">=" , "cmovae rax, "}, 	{"<=" , "cmovbe rax,"}
			};
		}
		return;
	}
} options("64", "out");



int getIndex (vector<string> array, string element)
{
	for (int i=0 ; i<array.size() ; ++i)
		if (array.at(i) == element)
			return i;
	return -1;
}

#endif
