#ifndef TOKEN_HEADER
#define TOKEN_HEADER


#include <string>
using namespace std;

class Token {
public:
	char		type;
	string	data;
	int		index;

	Token () {
		return;
	}

	Token (char type, string data, int index) {
		this->type = type;
		this->data = string(data);
		this->index = index;
		return;
	}

	bool match(char type, string data) {
		return (this->type==type && this->data==data);
	}
};


#endif
