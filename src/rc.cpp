#include "string.h"
#include "Error.hpp"
#include "Options.hpp"
#include "Compiler.hpp"

using namespace std;

int main(int args, char* argv[])
{
	if(args != 2) {
		printf("rc takes at least one argument\n");
		return 1;
	}

	char* document 	= (char*) malloc(1024*1024); //TODO: make this dynamic.
	char* line 			= (char*) malloc(1024);
	FILE* inputFile 	= fopen(argv[1], "rb");

	while (line=fgets(line,256,inputFile))
		strcat(document,line);
	fclose(inputFile);

	SyntaxTree code(document);
	Compiler compiler;

	puts( compiler.compile(code).c_str() );

	free(document);
	free(line);
	return 0;
}
