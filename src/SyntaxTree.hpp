#pragma once

#include <iostream>
#include <string>
#include <vector>
#include "Token.hpp"

using namespace std;

class SyntaxTree : public vector<Token>
{
	int		index;
	int		lastAccess;
	string	resChars= R"(()[]<>+-*/~$&=!?"'.,;)";

	vector<string>	operators
	{
			"(" , ")" , ";" , "+" , "-" , "*" , "/" , "$" , "&", "~" , "[" , "]" ,
			"<" , ">" , "<=" , ">=" , "==" , "!=" , "=" , "&&", "||",
			"+=" , "-=" , "*=" , "/=" , "$=" , "&=" , "," , ".", "++", "--"
	};

	vector<string>	keywords
	{
			"func", "if", "else", "while", "for", "ret", "iret",
			"break", "continue", "struct", "true", "false", "null", "new"
	};
	vector<string>	types
	{
			"v8", "v16", "v32", "v64",
			"s8", "s16", "s32", "s64",
			"u8", "u16", "u32", "u64",
			"pt"
	};


public:
	enum {
		OPERATOR = '+',
		KEYWORD	= 'k',
		TYPE		= 't',
		STRING	= 's',
		VARIABLE	= 'v',
		NUMBER	= '0'
	};

	SyntaxTree () : vector()
	{
		return;
	}

	SyntaxTree (string input) : vector()
	{
		index = 0;
		translate(input);
		return;
	}


	void translate (string input)
	{
		string 	temp;
		ulong		len		= input.size();

		for (int i=0 ; i<len ; i=i) {
			temp = "";
			if (isComment(input[i])) {
				++i;
				while (i<len && !isComment(input[i])) {
					++i;
				}
				++i;
			}
			else if ( isWhite(input[i]) ) {
				++i;
			}
			else if ( isStringOp(input[i]) ) {
				++i;
				while (i<len && !isStringOp(input[i]))
					temp += input[i++];
				++i;
				emplace_back(STRING, temp, i);
			}
			else if ( isAlpha(input[i]) ) {
				temp += input[i];
				char type = VARIABLE;
				++i;
				while( i<len && (isAlpha(input[i]) || isNum(input[i])) )
					temp += input[i++];

				if (find(keywords, temp))
					type = KEYWORD;
				if (find(types, temp))
					type = TYPE;
				emplace_back(type, temp, i);
			}
			else if ( isNum(input[i]) ) {
				while( i<len && isNum(input[i]) )
					temp += input[i++];
				emplace_back(NUMBER, temp, i);
			}
			else if ( isOpPartial(input[i]) ) {
				while( i<len && isOpPartial(input[i]) )	//get next block of operators.
					temp +=input[i++];
				while(temp.size() > 0)	{	//while operators remain, strip last symbol of.
					if( isOpFull(temp) )	{	//add when one matches.
						emplace_back(OPERATOR, temp, i);
						break;
					}
					else {
						temp.pop_back();
						--i;
					}
				}
			}
		}

		index = 0;
		return;
	}


	Token* getNext()
	{
		if(index < size()-1)
			return &at(index++);
		else
			return NULL;
	}

	Token* peek()
	{
		if(index < size())
			return &at(index);
		else
			return NULL;
	}

	Token* peek(uint n)
	{
		if( (index+n) < size() )
			return &at(index+n);
		else
			return NULL;
	}

	void revert()
	{
		if(index > 0)
			--index;
		return;
	}


	bool isAlpha(char input)
		{ return (input>='a' && input <='z') || (input>='A' && input<='Z') || (input=='_'); }

	bool isNum(char input)
		{ return input>='0' && input <='9'; }

	bool isWhite(char input)
		{ return (input=='\n') || (input=='\t') || (input==' '); }

	bool isNewline(char input)
		{ return input=='\n'; }

	bool isOpPartial(char input)
		{ return find(resChars, input); }

	bool isOpFull(string input)	//this one is filled with magic.
		{ return find(operators, input); }

	bool isStringOp(char input)
		{ return input=='\"'; }

	bool isComment(char input)
		{ return input=='#'; }

	bool find(vector<string> list, string element) {
		for(string t : list)
			if( t == element)
				return true;
		return false;
	}

	bool find(string list, char element) {
		for(char c : list)
			if( c == element)
				return true;
		return false;
	}
};
