#pragma once

#include <string>
#include "SyntaxTree.hpp"
using namespace std;

bool logging = true;

string RED (string input)   { return "\x1b[31;1m"+input+"\x1b[0m"; }
string BLUE (string input)  { return "\x1b[34;1m"+input+"\x1b[0m"; }
string GREEN (string input) { return "\x1b[32;1m"+input+"\x1b[0m"; }


void error (string msg, Token* tok) {
	cout << RED("Error:")+" [" << GREEN("data ") << tok->data << "  ";
	cout << GREEN("type ") << tok->type << "] " << BLUE(msg) << endl;
	return;
}


void log (const char* str, Token* tok) {
	if (!logging)
		return;
	cout << GREEN("[") << str << BLUE("@") << tok->index;
	cout << GREEN(" : ") << tok->data << GREEN("]") << "\n";
	return;
}

void log (const char* str, Token* tok, int depth) {
	if (!logging)
		return;
	for (int n=0 ; n<depth && n<100 ; ++n)
		cout << "  ";
	cout << GREEN("[") << str << BLUE("@") << tok->index;
	cout << GREEN(" : ") << tok->data << GREEN("]") << "\n";
	return;
}


void prelog (const char* str, Token* tok) {
	if (!logging)
		return;
	cout << GREEN("[") << str << BLUE("@") << tok->index << GREEN(" : ") << tok->data << GREEN("] -->");
	return;
}

void prelog (const char* str, Token* tok, int depth) {
	if (!logging)
		return;
	for (int n=0 ; n<depth && n<100 ; ++n)
		cout << "  ";
	cout << GREEN("[") << str << BLUE("@") << tok->index << GREEN(" : ") << tok->data << GREEN("] --> ");
	return;
}
